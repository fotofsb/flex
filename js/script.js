$(document).ready(function() {
  $('.portfolio-position').owlCarousel({
    loop: false,
    responsiveClass: true,
    dots: true,
    nav: false,
    responsive: {
      0: {
        items: 1,
        margin: 30,
        stagePadding: 30,
      },
      768: {
        items: 3,
        margin: 10,
        stagePadding: 10,
      },
      1280: {
        items: 3,
        margin: 40,
        stagePadding: 40,
      }
    }
  });
});


$(document).ready(function(){
  $('#nav-icon').click(function(){
    $(this).toggleClass('animate-icon');
    $('#overlay').fadeToggle();
  });
});

$(document).ready(function(){
  $('#overlay').click(function(){
    $('#nav-icon').removeClass('animate-icon');
    $('#overlay').toggle();
  });
});
